# EP2 - OO (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.


Para executar esta aplicacao primeiramente de clone neste repositorio, em seguida
com o netBeans abra o projeto EP2 e execute clicando na tecla F6. 

O programa é composto por 3 telas:

-Cadastro.
-Adição de produtos no estoque.
-Realização do pedido pelo funcionario.

Basta navegar entre os menus e adicionar os produtos.

