package Model;

import java.util.ArrayList;


public class Pedido {
   
    private Cliente cliente;
    private ArrayList<Prato> pratos;
    private ArrayList<Bebida> bebidas;
    private ArrayList<Sobremesa> sobremesas;
    private Float valorTotal;
    
    public Pedido(){
        pratos = new ArrayList<Prato>();
        bebidas = new ArrayList<Bebida>();
        sobremesas = new ArrayList<Sobremesa>();
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ArrayList<Prato> getPratos() {
        return pratos;
    }

    public void setPratos(ArrayList<Prato> pratos) {
        this.pratos = pratos;
    }

    public ArrayList<Bebida> getBebidas() {
        return bebidas;
    }

    public void setBebidas(ArrayList<Bebida> bebidas) {
        this.bebidas = bebidas;
    }

    public ArrayList<Sobremesa> getSobremesas() {
        return sobremesas;
    }

    public void setSobremesas(ArrayList<Sobremesa> sobremesas) {
        this.sobremesas = sobremesas;
    }

    public Float getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Float valorTotal) {
        this.valorTotal = valorTotal;
    }

}