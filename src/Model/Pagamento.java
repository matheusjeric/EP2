package Model;

abstract public class Pagamento {
    
    private boolean pagar;
    
    public boolean pago(){
        return pagar;
    }
    public void setPagar(boolean pagar){
        this.pagar = pagar;
    }
}
