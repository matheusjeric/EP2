package Controller;

import Model.Cliente;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;


public class ControleCliente {
    
    private ArrayList<Cliente> listaCliente;
    
    public ControleCliente(){
        this.listaCliente = new ArrayList<Cliente>();
    }

    public ArrayList<Cliente> getClientList() {
        return listaCliente;
    }
    
    public void add(Cliente aClient){
        listaCliente.add(aClient);
    }
    public void remove(Cliente aClient){
        listaCliente.remove(aClient);
    }
    public Cliente Search(String name){
        for(Cliente b: listaCliente){
            if(b.getNome().equalsIgnoreCase(name)) return b;
        }
        return null;
    }
    public void escreverArquivoCliente(){
        
        try {

            File file = new File("src/banco/Clientes.txt");

		
		if (!file.exists()) {
                    file.createNewFile();
		}

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		for(Cliente clt: listaCliente){
                    bw.write(clt.getNome() + ",");
                    bw.write("\n");
		
                }
                bw.flush();
                bw.close();

	} catch (IOException e) {
            e.printStackTrace();
	}
	
    }
    public void lerArquivoCliente(){
        ArrayList<Cliente> clientesLidos;
        clientesLidos = new ArrayList<Cliente>();
        
        Cliente cliente = null;
        
        String line = null;
        Scanner sc = null;
        try{
        sc = new Scanner(new File("src/banco/Clientes.txt"));
        }catch (FileNotFoundException e) {
            e.printStackTrace();  
        }
        while(sc.hasNext()){
            cliente = new Cliente();
            line = sc.nextLine();  
            
            String[] parts = line.split(",");
            String nome = parts[0];
            String cpf = parts[1];

            cliente.setNome(nome);
            
            clientesLidos.add(cliente);
            
        
        }
        listaCliente = (ArrayList<Cliente>) clientesLidos.clone();

    }
}