/*package Controller;

import Model.Sobremesa;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Float.parseFloat;
import java.util.ArrayList;
import java.util.Scanner;

public class ControleSobremesa {

    private ArrayList<Sobremesa> listaSobremesa;

    public ControleSobremesa() {
        this.listaSobremesa = new ArrayList<Sobremesa>();
    }

    public ArrayList<Sobremesa> getSobremesaList() {
        return listaSobremesa;
    }

    public void add(Sobremesa Sobremesa) {
        listaSobremesa.add(Sobremesa);
    }

    public void remove(Sobremesa Sobremesa) {
        listaSobremesa.remove(Sobremesa);
    }

public void lerArquivoSobremesas(){
        
        Sobremesa sobremesa = null;
        ArrayList<Sobremesa> listaSobremesaTemp = new ArrayList<Sobremesa>();
        String line = null;
        Scanner sc = null;
        try{
        sc = new Scanner(new File("src/banco/Sobremesas.txt"));
        }catch (FileNotFoundException e) {
            e.printStackTrace();  
        }
        while(sc.hasNext()){
            sobremesa = new Sobremesa();
            
            line = sc.nextLine();  
            
            String[] parts = line.split(",");
            String nome = parts[0];
            String valor = parts[1];
            
            sobremesa.setNome(nome);
            sobremesa.setValor(parseFloat(valor));
            
            
            listaSobremesaTemp.add(sobremesa);
        }
        listaSobremesa =  (ArrayList<Sobremesa>) listaSobremesaTemp.clone();

        
        }
        
    
    public void escreverArquivoSobremesa(){
        
        try {

            File file = new File("src/banco/Sobremesas.txt");
            
		
            if (!file.exists()) {
                file.createNewFile();
            }

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter buffer = new BufferedWriter(fw);
		for(Sobremesa sobre: listaSobremesa){
                    
                    
                   // buffer.write(sobre.getNome() + "," + sobre.getValor());
                    buffer.write("\n");
		
                }
                
                
                buffer.close();


	} catch (IOException e) {
            e.printStackTrace();
	}
	
    }
}
*/